/**
 * menu.cpp
 *
 * Gerenciador de Finan�as Pessoais
 *
 * Objetivo: Gerar as telas de menu do sistema, facilitando
 * a navega��o entre as funcionalidades.
 *
 * @author Jonathan W.
 * @version 1.0 19/08/2016
 */

#include "menu.h"

/// Constantes para uso no menu, determinam qual o menu atual.
const int PRINCIPAL = 0;
const int MEMBROS = 1;
const int DESPESAS = 2;
const int CREDITOS = 3;
const int RELATORIOS = 4;

/// Constantes para uso no CRUD.
const int LISTAR = 1;
const int CRIAR = 2;
const int EDITAR = 3;
const int REMOVER = 4;
const int VOLTAR = 9;

/// Constantes para uso em Relat�rios.
const int TOTAL_RECEBIDO = 1;
const int TOTAL_DESPESAS = 2;
const int SALDO_POUPAR = 3;
const int SOBRA_TOTAL = 4;
const int DIVIDAS_NAO_PAGAS = 5;
const int TOTAL_POUPANCA = 6;
const int FERIAS = 7;

using namespace std;

/*!
 * @discussion Mostra os itens do Menu Principal.
 *
 * @return void
 */
void mostraMenuPrincipal()
{
    vector<string> itens;
    itens.push_back(to_string(MEMBROS) + ". Membro Familiar.");
    itens.push_back(to_string(DESPESAS) + ". Despesas.");
    itens.push_back(to_string(CREDITOS) + ". Creditos.");
    itens.push_back(to_string(RELATORIOS) + ". Relatorios.");
    montaMenu("", itens, PRINCIPAL);
}

/*!
 * @discussion Mostra os itens do Menu Membros.
 *
 * @return void
 */
void mostraMembros()
{
    vector<string> itens;
    itens.push_back(to_string(LISTAR) + ". Listar Membros.");
    itens.push_back(to_string(CRIAR) + ". Criar Membro.");
    itens.push_back(to_string(EDITAR) + ". Editar Membro.");
    itens.push_back(to_string(REMOVER) + ". Remover Membro.");
    montaMenu("Cadastro de Membros Familiares > Menu", itens, MEMBROS);
}

/*!
 * @discussion Mostra os itens do Menu Despesas.
 *
 * @return void
 */
void mostraDespesas()
{
    vector<string> itens;
    itens.push_back(to_string(LISTAR) + ". Listar Despesas.");
    itens.push_back(to_string(CRIAR) + ". Criar Despesa.");
    itens.push_back(to_string(EDITAR) + ". Editar Despesa.");
    itens.push_back(to_string(REMOVER) + ". Remover Despesa.");
    montaMenu("Cadastro de Despesas > Menu", itens, DESPESAS);
}

/*!
 * @discussion Mostra os itens do Menu Creditos.
 *
 * @return void
 */
void mostraCreditos()
{
    vector<string> itens;
    itens.push_back(to_string(LISTAR) + ". Listar Creditos.");
    itens.push_back(to_string(CRIAR) + ". Criar Credito.");
    itens.push_back(to_string(EDITAR) + ". Editar Credito.");
    itens.push_back(to_string(REMOVER) + ". Remover Credito.");
    montaMenu("Cadastro de Creditos > Menu", itens, CREDITOS);
}

/*!
 * @discussion Mostra os itens do Menu Relat�rios.
 *
 * @return void
 */
void mostraRelatorios()
{
    vector<string> itens;
    itens.push_back(to_string(TOTAL_RECEBIDO) + ". Total Recebido.");
    itens.push_back(to_string(TOTAL_DESPESAS) + ". Total de Despesas.");
    itens.push_back(to_string(SALDO_POUPAR) + ". Saldo a Poupar.");
    itens.push_back(to_string(SOBRA_TOTAL) + ". Sobra Total.");
    itens.push_back(to_string(DIVIDAS_NAO_PAGAS) + ". Dividas Nao Pagas.");
    itens.push_back(to_string(TOTAL_POUPANCA) + ". Saldo Total em Poupanca.");
    itens.push_back(to_string(FERIAS) + ". Planejamento de Ferias.");
    montaMenu("Relatorios > Menu", itens, RELATORIOS);
}

/*!
 * @discussion Imprime o item do menu na tela.
 *
 * @param item (string) O texto do item a ser impresso.
 *
 * @return void
 */
void imprimeItens(string item)
{
    cout << "| " << item << setw(48 - (int)item.length()) << " |" << endl;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e executa a
 * fun��o de acordo com ela.
 * Op��es especificas do Menu Principal.
 *
 * @param opt (int) Op��o escolhida.
 *
 * @return bool Retorna verdadeiro se conseguiu executar, e falso se a op��o escolhida n�o existir.
 */
bool opcoesPrincipal(int opt)
{
    switch (opt)
    {
        case MEMBROS:
            mostraMembros();
            break;
        case DESPESAS:
            mostraDespesas();
            break;
        case CREDITOS:
            mostraCreditos();
            break;
        case RELATORIOS:
            mostraRelatorios();
            break;
        case VOLTAR:
            break;
        default:
            return false;
    }
    return true;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e executa a
 * fun��o de acordo com ela.
 * Op��es especificas do Menu Membros.
 *
 * @param opt (int) Op��o escolhida.
 *
 * @return bool Retorna verdadeiro se conseguiu executar, e falso se a op��o escolhida n�o existir.
 */
bool opcoesMembros(int opt)
{
    switch (opt)
    {
        case LISTAR:
            listaMembros();
            break;
        case CRIAR:
            criaMembro();
            break;
        case EDITAR:
            alteraMembro();
            break;
        case REMOVER:
            excluiMembro();
            break;
        case VOLTAR:
            mostraMenuPrincipal();
            break;
        default:
            return false;
    }
    return true;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e executa a
 * fun��o de acordo com ela.
 * Op��es especificas do Menu Despesas.
 *
 * @param opt (int) Op��o escolhida.
 *
 * @return bool Retorna verdadeiro se conseguiu executar,e falso se a op��o escolhida n�o existir.
 */
bool opcoesDespesas(int opt)
{
    switch (opt)
    {
        case LISTAR:
            listaDespesas();
            break;
        case CRIAR:
            criaDespesa();
            break;
        case EDITAR:
            alteraDespesa();
            break;
        case REMOVER:
            excluiDespesa();
            break;
        case VOLTAR:
            mostraMenuPrincipal();
            break;
        default:
            return false;
    }
    return true;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e executa a
 * fun��o de acordo com ela.
 * Op��es especificas do Menu Creditos.
 *
 * @param opt (int) Op��o escolhida.
 *
 * @return bool Retorna verdadeiro se conseguiu executar, e falso se a op��o escolhida n�o existir.
 */
bool opcoesCreditos(int opt)
{
    switch (opt)
    {
        case LISTAR:
            listaCreditos();
            break;
        case CRIAR:
            criaCredito();
            break;
        case EDITAR:
            alteraCredito();
            break;
        case REMOVER:
            excluiCredito();
            break;
        case VOLTAR:
            mostraMenuPrincipal();
            break;
        default:
            return false;
    }
    return true;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e executa a
 * fun��o de acordo com ela.
 * Op��es especificas do Menu Relat�rios.
 *
 * @param opt (int) Op��o escolhida.
 *
 * @return bool Retorna verdadeiro se conseguiu executar, e falso se a op��o escolhida n�o existir.
 */
bool opcoesRelatorios(int opt)
{
    switch (opt)
    {
        case TOTAL_RECEBIDO:
            totalRecebido();
            break;
        case TOTAL_DESPESAS:
            totalDespesas();
            break;
        case SALDO_POUPAR:
            saldoPoupar();
            break;
        case SOBRA_TOTAL:
            sobraTotal();
            break;
        case DIVIDAS_NAO_PAGAS:
            dividasNaoPagas();
            break;
        case TOTAL_POUPANCA:
            totalPoupanca();
            break;
        case FERIAS:
            ferias();
            break;
        case VOLTAR:
            mostraMenuPrincipal();
            break;
        default:
            return false;
    }
    return true;
}

/*!
 * @discussion Verifica a op��o escolhida pelo usu�rio e o menu em que
 * ele atualmente est�.
 * Dispacha as fun��es necess�rias e espera o retorno das
 * fun��es para determina se houve sucesso ou falha.
 *
 * @param opt (int) Op��o escolhida.
 * @param menu (int) Menu atual.
 *
 * @return bool Retorna verdadeiro se conseguiu executar, e falso se a op��o ou menu escolhido n�o existir.
 */
bool chamaSwitch(int opt, int menu)
{
    switch (menu) {
        case PRINCIPAL:
            return opcoesPrincipal(opt);
            break;
        case MEMBROS:
            return opcoesMembros(opt);
            break;
        case DESPESAS:
            return opcoesDespesas(opt);
            break;
        case CREDITOS:
            return opcoesCreditos(opt);
            break;
        case RELATORIOS:
            return opcoesRelatorios(opt);
            break;
        default:
            return false;
            break;
    }
    return true;
}

/*!
 * @discussion Monta o visual do menu na tela de acordo com os dados
 * recebidos via parametro.
 *
 * @param bread (string) Texto que ser� exibido no breadcrumbs presente no cabe�alho do programa.
 * @param itens (vector<string>) Lista de itens do menu, para que sejam montadas na tela.
 * @param menu  (int) Contante indicando qual o menu atual em tela.
 *
 * @return void
 */
void montaMenu(string bread, vector<string> itens, int menu)
{
    string opcao = "";
    int opt = 0;
    
    // Imprime o Cabe�alho do programa.
    mostraCabecalho(bread);
    
    cout << "#================================================#" << endl;
    for_each(itens.begin(), itens.end(), imprimeItens);
    cout << "|                                                |" << endl;
    if (menu == PRINCIPAL) cout << "| " << VOLTAR << ". Sair do Sistema.                            |" << endl;
    else cout << "| " << VOLTAR << ". Voltar ao Menu Anterior.                    |" << endl;
    cout << "#================================================#" << endl << endl
         << "Selecione a opcao desejada: ";
    
    getline(cin, opcao);
    
    stringstream myStream(opcao);
   	if (myStream >> opt)
    {
        istringstream buffer(opcao);
        buffer >> opt;
        if (chamaSwitch(opt, menu)) exit(0);
    }
    wcout << "Opcao invalida, pressione enter para tentar novamente. ";
    getline(cin, opcao);
    montaMenu(bread, itens, menu);
}

/*!
 * @discussion Monta o visual do cabe�alho de acordo com a tela atual.
 * Pode receber como parametro o breadcrumbs da tela.
 *
 * @param item (string) Texto que ser� exibido no breadcrumbs presente no cabe�alho do programa.
 *
 * @return void
 */
void mostraCabecalho(string item)
{
    limpaConsole();
	cout << "#================================================#" << endl
         << "| Gerenciador Financeiro v1.0                    |" << endl
         << "|                                                |" << endl;
    if (item.length() > 0) {
        cout << "| " << item << setw(48 - (int)item.length()) << " |" << endl
             << "#================================================#" << endl << endl;
    } else {
        cout << "| Bem vindo ao Gerenciador de Despesas Familiar  |" << endl
              << "| seu software de controle financeiro pessoal.   |" << endl
              << "#================================================#" << endl << endl;
    }
    
}

/*!
 * @discussion Limpa o console. Tem uma a��o diferente dependendo do
 * sistema operacional em que foi compilado.
 *
 * @return void
 */
void limpaConsole()
{
    #ifdef _WIN32
        system("cls");
        #ifdef _WIN64
            system("cls");
        #endif
    #elif __APPLE__
        #include "TargetConditionals.h"
        #if TARGET_IPHONE_SIMULATOR
            system("clear");
        #elif TARGET_OS_IPHONE
            system("clear");
        #elif TARGET_OS_MAC
            system("clear");
        #else
            #   error "Unknown Apple platform"
        #endif
    #elif __linux__
        system("clear");
    #elif __unix__ // all unices not caught above
        system("clear");
    #elif defined(_POSIX_VERSION)
        system("clear");
    #else
        #   error "Unknown compiler"
    #endif
}