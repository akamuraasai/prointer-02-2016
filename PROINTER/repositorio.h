#include "estruturas.h"

bool extraiData(const std::string& s, int& d, int& m, int& y);
bool validaDataMenor(s_data inicial, s_data final);
void limpaBanco();
void criarTabelas();
//bool inserirRegistro(int op, const char* nome, int idade, const char* tipo);
bool inserirRegistro(int op, s_membro membro =  s_membro(), s_despesa despesa = s_despesa(), s_credito credito = s_credito());
bool alterarRegistro(int op, s_membro membro = s_membro(), s_despesa despesa = s_despesa(), s_credito credito = s_credito());
bool alterarRegistro(int op, int id, const char* nome, int idade, const char* tipo);
bool listarRegistros(int op, std::vector<std::vector<std::string>> &retorno);
bool retornaRegistro(int op, int id, std::vector<std::vector<std::string>> &retorno);
bool excluirRegistro(int op, int id);