#include "menu.h"

using namespace std;

/*!
 * @discussion Imprime a lista de despesas no sistema.
 *
 * @return void
 */
void listaDespesas()
{
    string opcao = "";
    std::vector<std::vector<std::string>> dados;
    
    mostraCabecalho("Cadastro de Despesas > Lista");
    
    if (listarRegistros(2, dados))
    {
        
        cout << "#================================================#" << endl
             << "|  id | Pago | Descricao   |  Valor |    Venc.   |" << endl
             << "--------------------------------------------------" << endl;
        
        for (int i = 0; i < dados[0].size(); i++) {
            string id = dados[0][i];
            int id_t = (4 - (int)id.length() >= 0 ? 4 - (int)id.length() : 0);
            
            string desc = dados[1][i];
            int desc_t = (14 - (int)desc.length() >= 0 ? 14 - (int)desc.length() : 0);
            
            string venc = dados[4][i];
            int venc_t = (8 - (int)venc.length() >= 0 ? 8 - (int)venc.length() : 0);
            
            size_t j(0);
            float f = stof(dados[6][i], &j);
            float aux_valor = roundf(f * 100) / 100;
            char aux[20];
            sprintf (aux, "%.2f", aux_valor);
            
            string valor(aux);
            int valor_t = (8 - (int)valor.length() >= 0 ? 8 - (int)valor.length() : 0);
            
            string pago = dados[5][i];
            pago = !pago.compare(" ") ? "SIM" : "NAO";
            int pago_t = (7 - (int)pago.length() >= 0 ? 7 - (int)pago.length() : 0);
            
            cout << "| " << setw(id_t) << id << " | " << pago << setw(pago_t) << " | " << desc << setw(desc_t) << " | " << valor << setw(valor_t) << " | " << venc << setw(venc_t) << " |" << endl;
        }
        
        cout << "#================================================#" << endl << endl
        << "Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << "Nenhum registro encontrado. Pressione Enter para voltar ao menu." << endl;
    }
    
    getline(cin, opcao);
    mostraDespesas();
}

/*!
 * @discussion  Cria uma despesa no sistema, utilizando
 * os dados que o usuário digitar.
 *
 * @return void
 */
void criaDespesa()
{
    string descricao = "";
    string categoria = "";
    string valor = "";
    string idade = "";
    string tipo = "";
    string venc = "";
    string id_membro = "";
    s_despesa despesa = {0, "", "", "", "", "", 0.0, 0.0, 0, 0};
    std::vector<std::vector<std::string>> dados_membro;
    bool continua;
    int num = 0;
    
    mostraCabecalho("Cadastro de Despesas > Cadastrar");
    
    do
    {
        continua = false;
        cout << " Escolha o tipo de despesa (P - Pessoal / F - Familia): ";
        getline(cin, tipo);
        
        if (tipo.compare("P") == 0 || tipo.compare("p") == 0 || tipo.compare("F") == 0 || tipo.compare("f") == 0) continua = true;
    } while(!continua);
    despesa.tipo = tipo.c_str();
    
    if (tipo.compare("P") == 0 || tipo.compare("p") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite o id do membro familiar: ";
            getline(cin, id_membro);
            
            stringstream myStream(id_membro);
            if (myStream >> num)
            {
                istringstream buffer(id_membro);
                buffer >> num;
                if (retornaRegistro(1, num, dados_membro)) continua = true;
                else cout << " Membro familiar não encontrado. Tente novamente." << endl;
            }
        } while(!continua);
        despesa.id_membro = num;
    }
    
    do
    {
        continua = false;
        cout << " Digite a descrição da despesa: ";
        getline(cin, descricao);
        
        despesa.descricao = descricao.c_str();
        continua = true;
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite a categoria da despesa: ";
        getline(cin, categoria);
        
        if (categoria.length() >= 6) {
            despesa.categoria = categoria.c_str();
            continua = true;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite o valor da despesa: ";
        getline(cin, valor);
        
        try{
            size_t i(0);
            float f = stof(valor, &i);
            despesa.valor = roundf(f * 100) / 100;
            continua = true;
        } catch( ... )
        {
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite data em que a despesa vence: ";
        getline(cin, venc);
        int d,m,y;
        
        if (extraiData(venc, d, m, y))
        {
            despesa.dt_venc = venc.c_str();
            continua = true;
        }
    } while(!continua);
    
    if (inserirRegistro(2, s_membro(), despesa))
    {
        cout << endl << " Despesa cadastrada com sucesso. Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << endl << " Erro durante cadastro, tente novamente. Pressione Enter para voltar ao menu.";
    }
    
    getline(cin, descricao);
    mostraDespesas();
}

/*!
 * @discussion Altera uma despesa cadastrada no sistema, 
 * baseado no id digitado pelo usuário.
 *
 * @return void
 */
void alteraDespesa()
{
    string id = "";
    string descricao = "";
    string categoria = "";
    string valor = "";
    string idade = "";
    string tipo = "";
    string venc = "";
    string id_membro = "";
    s_despesa despesa = {0, "", "", "", "", "", 0.0, 0.0, 0, 0};
    std::vector<std::vector<std::string>> dados_membro;
    std::vector<std::vector<std::string>> dados;
    bool continua;
    int num = 0;
    
    mostraCabecalho("Cadastro de Despesas > Alterar");
    
    do
    {
        continua = false;
        cout << " Digite o id da despesa: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(2, num, dados)) continua = true;
            else cout << " Despesa não encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    despesa.id = num;
    
    do
    {
        continua = false;
        cout << " Escolha o tipo de despesa (P - Pessoal / F - Familia): ";
        getline(cin, tipo);
        
        if (tipo.compare("P") == 0 || tipo.compare("p") == 0 || tipo.compare("F") == 0 || tipo.compare("f") == 0) continua = true;
    } while(!continua);
    despesa.tipo = tipo.c_str();
    
    if (tipo.compare("P") == 0 || tipo.compare("p") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite o id do membro familiar: ";
            getline(cin, id_membro);
            
            stringstream myStream(id_membro);
            if (myStream >> num)
            {
                istringstream buffer(id_membro);
                buffer >> num;
                if (retornaRegistro(1, num, dados_membro)) continua = true;
                else cout << " Membro familiar não encontrado. Tente novamente." << endl;
            }
        } while(!continua);
        despesa.id_membro = num;
    }
    
    do
    {
        continua = false;
        cout << " Digite a descrição da despesa: ";
        getline(cin, descricao);
        
        despesa.descricao = descricao.c_str();
        continua = true;
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite a categoria da despesa: ";
        getline(cin, categoria);
        
        if (categoria.length() >= 6) {
            despesa.categoria = categoria.c_str();
            continua = true;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite o valor da despesa: ";
        getline(cin, valor);
        
        try{
            size_t i(0);
            float f = stof(valor, &i);
            despesa.valor = roundf(f * 100) / 100;
            continua = true;
        } catch( ... )
        {
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite data em que a despesa vence: ";
        getline(cin, venc);
        int d,m,y;
        
        if (extraiData(venc, d, m, y))
        {
            despesa.dt_venc = venc.c_str();
            continua = true;
        }
    } while(!continua);
    
    
    if (alterarRegistro(2, s_membro(), despesa)) cout << endl << " Despesa alterada com sucesso. Pressione Enter para voltar ao menu.";
    else cout << endl << " Erro ao alterar despesa. Pressione Enter para voltar ao menu.";
    
    getline(cin, descricao);
    mostraDespesas();
}

/*!
 * @discussion Exclui uma despesa do sistema
 * baseado no id digitado pelo usuário.
 *
 * @return void
 */
void excluiDespesa()
{
    string id = "";
    int num = 0;
    bool continua;
    std::vector<std::vector<std::string>> dados;
    
    mostraCabecalho("Cadastro de Despesas > Excluir");
    
    do
    {
        continua = false;
        cout << " Digite o id da despesa: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(2, num, dados)) continua = true;
            else cout << " Despesa não encontrada. Tente novamente." << endl;
        }
    } while(!continua);
    
    if (excluirRegistro(2, num)) cout << endl << " Despesa excluida com sucesso. Pressione Enter para voltar ao menu.";
    else cout << endl << " Erro ao remover despesa. Pressione Enter para voltar ao menu.";
    
    getline(cin, id);
    mostraDespesas();
}
