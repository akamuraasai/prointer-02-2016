#include "menu.h"

using namespace std;

/*!
 * @discussion Imprime a lista de creditos no sistema.
 *
 * @return void
 */
void listaCreditos()
{
    string opcao = "";
    std::vector<std::vector<std::string>> dados;
    
    mostraCabecalho("Cadastro de Creditos > Lista");
    
    if (listarRegistros(3, dados))
    {
        
        cout << "#================================================#" << endl
        << "|  id | Tipo | Descricao   |  Valor |   Receb.   |" << endl
        << "--------------------------------------------------" << endl;
        
        for (int i = 0; i < dados[0].size(); i++) {
            string id = dados[0][i];
            int id_t = (4 - (int)id.length() >= 0 ? 4 - (int)id.length() : 0);
            
            string desc = dados[1][i];
            int desc_t = (14 - (int)desc.length() >= 0 ? 14 - (int)desc.length() : 0);
            
            string receb = dados[3][i];
            int receb_t = (8 - (int)receb.length() >= 0 ? 8 - (int)receb.length() : 0);
            
            size_t j(0);
            float f = stof(dados[6][i], &j);
            float aux_valor = roundf(f * 100) / 100;
            char aux[20];
            sprintf (aux, "%.2f", aux_valor);
            
            string valor(aux);
            int valor_t = (8 - (int)valor.length() >= 0 ? 8 - (int)valor.length() : 0);
            
            string tipo = dados[2][i];
            int tipo_t = (7 - (int)tipo.length() >= 0 ? 7 - (int)tipo.length() : 0);
            
            cout << "| " << setw(id_t) << id << " | " << tipo << setw(tipo_t) << " | " << desc << setw(desc_t) << " | " << valor << setw(valor_t) << " | " << receb << setw(receb_t) << " |" << endl;
        }
        
        cout << "#================================================#" << endl << endl
        << "Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << "Nenhum registro encontrado. Pressione Enter para voltar ao menu." << endl;
    }
    
    getline(cin, opcao);
    mostraCreditos();
}

/*!
 * @discussion  Cria um credito no sistema, utilizando
 * os dados que o usuário digitar.
 *
 * @return void
 */
void criaCredito()
{
    string descricao = "";
    string categoria = "";
    string valor = "";
    string valor_p = "";
    string tipo = "";
    string devol = "";
    string receb = "";
    string id_membro = "";
    s_credito credito = {0, "", "", "", "", 0.0, 0.0, 0};
    s_data dt_receb = {0, 0, 0};
    s_data dt_devol = {0, 0, 0};
    std::vector<std::vector<std::string>> dados_membro;
    bool continua;
    bool salario = false;
    bool emprestimo = false;
    int num = 0;
    
    mostraCabecalho("Cadastro de Creditos > Cadastrar");
    
    do
    {
        continua = false;
        cout << " Digite o id do membro familiar: ";
        getline(cin, id_membro);
        
        stringstream myStream(id_membro);
        if (myStream >> num)
        {
            istringstream buffer(id_membro);
            buffer >> num;
            if (retornaRegistro(1, num, dados_membro)) continua = true;
            else cout << " Membro familiar não encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    credito.id_membro = num;

    do
    {
        continua = false;
        cout << " Digite o valor do credito: ";
        getline(cin, valor);
        
        try{
            size_t i(0);
            float f = stof(valor, &i);
            credito.valor = roundf(f * 100) / 100;
            continua = true;
        } catch( ... )
        {
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite data em que recebera o dinheiro: ";
        getline(cin, receb);
        
        if (extraiData(receb, dt_receb.d, dt_receb.m, dt_receb.a))
        {
            credito.dt_receb = receb.c_str();
            continua = true;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Escolha o tipo de credito (S - Salario / P - Também Poupar / E - Emprestimo / O - Outros): ";
        getline(cin, tipo);
        
        if (tipo.compare("S") == 0 || tipo.compare("s") == 0 || tipo.compare("E") == 0 ||
            tipo.compare("e") == 0 || tipo.compare("O") == 0 || tipo.compare("o") == 0 ||
            tipo.compare("P") == 0 || tipo.compare("p") == 0) continua = true;
    } while(!continua);
    credito.tipo = tipo.c_str();
    
    if (tipo.compare("S") == 0 || tipo.compare("s") == 0)
    {
        salario = true;
    }
    else if (tipo.compare("P") == 0 || tipo.compare("p") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite o valor que será poupado: ";
            getline(cin, valor_p);
            
            try{
                size_t i(0);
                float f = stof(valor_p, &i);
                credito.valor_p = roundf(f * 100) / 100;
                if (credito.valor_p <= credito.valor) continua = true;
            } catch( ... )
            {
            }
        } while(!continua);
    }
    else if (tipo.compare("E") == 0 || tipo.compare("e") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite data em que será devolvido o dinheiro: ";
            getline(cin, devol);
            
            if (extraiData(devol, dt_devol.d, dt_devol.m, dt_devol.a))
            {
                credito.dt_devol = devol.c_str();
                emprestimo = true;
                if (validaDataMenor(dt_receb, dt_devol)) continua = true;
            }
        } while(!continua);
    }
    
    do
    {
        continua = false;
        cout << " Digite a descrição do credito: ";
        getline(cin, descricao);
        
        credito.descricao = descricao.c_str();
        continua = true;
    } while(!continua);
    
    if (inserirRegistro(3, s_membro(), s_despesa(), credito))
    {
        if (emprestimo) {
            s_despesa despesa = {0, "Emprestimo", "Emprestimo", "P", credito.dt_devol, "", credito.valor, 0.0, credito.id_membro, credito.id};
            inserirRegistro(2, s_membro(), despesa);
        }
        cout << endl << " Credito cadastrado com sucesso. Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << endl << " Erro durante cadastro, tente novamente. Pressione Enter para voltar ao menu.";
    }
    
    getline(cin, descricao);
    mostraCreditos();
}

/*!
 * @discussion Altera uma despesa cadastrada no sistema,
 * baseado no id digitado pelo usuário.
 *
 * @return void
 */
void alteraCredito()
{
    string id = "";
    string descricao = "";
    string categoria = "";
    string valor = "";
    string valor_p = "";
    string tipo = "";
    string devol = "";
    string receb = "";
    string id_membro = "";
    s_credito credito = {0, "", "", "", "", 0.0, 0.0, 0};
    s_data dt_receb = {0, 0, 0};
    s_data dt_devol = {0, 0, 0};
    std::vector<std::vector<std::string>> dados_membro;
    std::vector<std::vector<std::string>> dados;
    bool continua;
    bool salario = false;
    bool emprestimo = false;
    int num = 0;
    
    mostraCabecalho("Cadastro de Creditos > Alterar");
    
    do
    {
        continua = false;
        cout << " Digite o id do credito: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(3, num, dados)) continua = true;
            else cout << " Credito não encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    credito.id = num;
    
    
    do
    {
        continua = false;
        cout << " Digite o id do membro familiar: ";
        getline(cin, id_membro);
        
        stringstream myStream(id_membro);
        if (myStream >> num)
        {
            istringstream buffer(id_membro);
            buffer >> num;
            if (retornaRegistro(1, num, dados_membro)) continua = true;
            else cout << " Membro familiar não encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    credito.id_membro = num;
    
    do
    {
        continua = false;
        cout << " Digite o valor do credito: ";
        getline(cin, valor);
        
        try{
            size_t i(0);
            float f = stof(valor, &i);
            credito.valor = roundf(f * 100) / 100;
            continua = true;
        } catch( ... )
        {
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite data em que recebera o dinheiro: ";
        getline(cin, receb);
        
        if (extraiData(receb, dt_receb.d, dt_receb.m, dt_receb.a))
        {
            credito.dt_receb = receb.c_str();
            continua = true;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Escolha o tipo de credito (S - Salario / P - Também Poupar / E - Emprestimo / O - Outros): ";
        getline(cin, tipo);
        
        if (tipo.compare("S") == 0 || tipo.compare("s") == 0 || tipo.compare("E") == 0 ||
            tipo.compare("e") == 0 || tipo.compare("O") == 0 || tipo.compare("o") == 0 ||
            tipo.compare("P") == 0 || tipo.compare("p") == 0) continua = true;
    } while(!continua);
    credito.tipo = tipo.c_str();
    
    if (tipo.compare("S") == 0 || tipo.compare("s") == 0)
    {
        salario = true;
    }
    else if (tipo.compare("P") == 0 || tipo.compare("p") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite o valor que será poupado: ";
            getline(cin, valor_p);
            
            try{
                size_t i(0);
                float f = stof(valor_p, &i);
                credito.valor_p = roundf(f * 100) / 100;
                if (credito.valor_p <= credito.valor) continua = true;
            } catch( ... )
            {
            }
        } while(!continua);
    }
    else if (tipo.compare("E") == 0 || tipo.compare("e") == 0)
    {
        do
        {
            continua = false;
            cout << " Digite data em que será devolvido o dinheiro: ";
            getline(cin, devol);
            
            if (extraiData(devol, dt_devol.d, dt_devol.m, dt_devol.a))
            {
                credito.dt_devol = devol.c_str();
                emprestimo = true;
                if (validaDataMenor(dt_receb, dt_devol)) continua = true;
            }
        } while(!continua);
    }
    
    do
    {
        continua = false;
        cout << " Digite a descrição do credito: ";
        getline(cin, descricao);
        
        credito.descricao = descricao.c_str();
        continua = true;
    } while(!continua);
    
    if (inserirRegistro(3, s_membro(), s_despesa(), credito))
    {
        if (emprestimo) {
            s_despesa despesa = {0, "Emprestimo", "Emprestimo", "P", credito.dt_devol, "", credito.valor, 0.0, credito.id_membro, credito.id};
            inserirRegistro(2, s_membro(), despesa);
        }
        cout << endl << " Credito cadastrado com sucesso. Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << endl << " Erro durante cadastro, tente novamente. Pressione Enter para voltar ao menu.";
    }
    getline(cin, descricao);
    mostraCreditos();
}

/*!
 * @discussion Exclui um credito do sistema
 * baseado no id digitado pelo usuário.
 *
 * @return void
 */
void excluiCredito()
{
    string id = "";
    int num = 0;
    bool continua;
    std::vector<std::vector<std::string>> dados;
    
    mostraCabecalho("Cadastro de Creditos > Excluir");
    
    do
    {
        continua = false;
        cout << " Digite o id do credito: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(3, num, dados)) continua = true;
            else cout << " Credito não encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    
    if (excluirRegistro(3, num)) cout << endl << " Credito excluido com sucesso. Pressione Enter para voltar ao menu.";
    else cout << endl << " Erro ao remover credito. Pressione Enter para voltar ao menu.";
    
    getline(cin, id);
    mostraCreditos();
}
