#include "menu.h"

using namespace std;

/*!
 * @discussion Imprime a lista de membros cadastrados
 * no sistema.
 *
 * @return void
 */
void listaMembros()
{
    string opcao = "";
    std::vector<std::vector<std::string>> dados;
    
	mostraCabecalho("Cadastro de Membros Familiares > Lista");
    
    if (listarRegistros(1, dados))
    {
        
        cout << "#================================================#" << endl
             << "|  id | Nome                    | Idade | Grupo  |" << endl
             << "--------------------------------------------------" << endl;
        
        for (int i = 0; i < dados[0].size(); i++) {
            string id = dados[0][i];
            int id_t = (4 - (int)id.length() >= 0 ? 4 - (int)id.length() : 0);
            
            string nome = dados[1][i];
            int nome_t = (26 - (int)nome.length() >= 0 ? 26 - (int)nome.length() : 0);
            
            string idade = dados[2][i];
            int idade_t = (7 - (int)idade.length() >= 0 ? 7 - (int)idade.length() : 0);
            
            string grupo = (dados[3][i] == "F" || dados[3][i] == "f" ? "Filhos" : "Pais");
            int grupo_t = (8 - (int)grupo.length() >= 0 ? 8 - (int)grupo.length() : 0);
            
            cout << "| " << setw(id_t) << id << " | " << nome << setw(nome_t) << " | " << setw(idade_t) << idade << " | " << grupo << setw(grupo_t) << " |" << endl;
        }
        
        cout << "#================================================#" << endl << endl
        << "Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << "Nenhum registro encontrado. Pressione Enter para voltar ao menu." << endl;
    }
    
    getline(cin, opcao);
	mostraMembros();
}

/*!
 * @discussion  Cria um membro no sistema, utilizando
 * os dados que o usu�rio digitar.
 *
 * @return void
 */
void criaMembro()
{
	string nome = "";
    string idade = "";
    string grupo = "";
    bool continua;
    int num = 0;
//    vector<s_membro> membro;
	
	mostraCabecalho("Cadastro de Membros Familiares > Cadastrar");
	
    do
    {
        continua = false;
        cout << " Digite o nome do membro familiar: ";
        getline(cin, nome);
        
        if (nome.length() >= 6) continua = true;
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite a idade do membro familiar: ";
        getline(cin, idade);
        
        stringstream myStream(idade);
        if (myStream >> num)
        {
            istringstream buffer(idade);
            buffer >> num;
            continua = true;
        }
    } while(!continua);
	
    do
    {
        continua = false;
        cout << " Digite o grupo ao qual pertence o membro (P - Pais / F - Filhos): ";
        getline(cin, grupo);
        
        if (grupo.compare("P") == 0 || grupo.compare("p") == 0 || grupo.compare("F") == 0 || grupo.compare("f") == 0) continua = true;
    } while(!continua);
	
    s_membro membro = { 0, nome.c_str(), num, grupo.c_str() };
    cout << endl << endl
    << " Nome: " << membro.nome << endl
    << " Idade: " << membro.idade << endl
    << " Grupo: " << membro.tipo << endl;
    
    if (inserirRegistro(1, membro))
    {
        cout << endl << " Membro familiar cadastrado com sucesso. Pressione Enter para voltar ao menu.";
    }
    else
    {
        cout << endl << " Erro durante cadastro, tente novamente. Pressione Enter para voltar ao menu.";
    }
    
	getline(cin, nome);
	mostraMembros();
}

/*!
 * @discussion Altera um membro familiar cadastrado
 * no sistema, baseado no id digitado
 * pelo usu�rio.
 *
 * @return void
 */
void alteraMembro()
{
    string id = "";
    string nome = "";
    string idade = "";
    string grupo = "";
    bool continua;
    int num = 0;
    int aux = 0;
    std::vector<std::vector<std::string>> dados;
    
    mostraCabecalho("Cadastro de Membros Familiares > Alterar");
    
    do
    {
        continua = false;
        cout << " Digite o id do membro familiar: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(1, num, dados)) continua = true;
            else cout << " Membro familiar n�o encontrado. Tente novamente." << endl;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite o nome do membro familiar: ";
        getline(cin, nome);
        
        if (nome.length() >= 6) continua = true;
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite a idade do membro familiar: ";
        getline(cin, idade);
        
        stringstream myStream(idade);
        if (myStream >> aux)
        {
            istringstream buffer(idade);
            buffer >> aux;
            continua = true;
        }
    } while(!continua);
    
    do
    {
        continua = false;
        cout << " Digite o grupo ao qual pertence o membro (P - Pais / F - Filhos): ";
        getline(cin, grupo);
        
        if (grupo.compare("P") == 0 || grupo.compare("p") == 0 || grupo.compare("F") == 0 || grupo.compare("f") == 0) continua = true;
    } while(!continua);
    
    s_membro membro = { num, nome.c_str(), aux, grupo.c_str() };
    cout << endl << endl
    << " Nome: " << membro.nome << endl
    << " Idade: " << membro.idade << endl
    << " Grupo: " << membro.tipo << endl;
    
    if (alterarRegistro(1, membro)) cout << endl << " Membro familiar alterado com sucesso. Pressione Enter para voltar ao menu.";
    else cout << endl << " Erro ao alterar membro familiar. Pressione Enter para voltar ao menu.";
    
    getline(cin, nome);
    mostraMembros();
}

/*!
 * @discussion Exclui um membro familiar do sistema
 * baseado no id digitado pelo usu�rio.
 *
 * @return void
 */
void excluiMembro()
{
	string id = "";
    int num = 0;
    bool continua;
    std::vector<std::vector<std::string>> dados;
	
	mostraCabecalho("Cadastro de Membros Familiares > Excluir");
	
    do
    {
        continua = false;
        cout << " Digite o id do membro familiar: ";
        getline(cin, id);
        
        stringstream myStream(id);
        if (myStream >> num)
        {
            istringstream buffer(id);
            buffer >> num;
            if (retornaRegistro(1, num, dados)) continua = true;
            else cout << " Membro familiar n�o encontrado. Tente novamente." << endl;
        }
    } while(!continua);

    if (excluirRegistro(1, num)) cout << endl << " Membro familiar excluido com sucesso. Pressione Enter para voltar ao menu.";
    else cout << endl << " Erro ao remover membro familiar. Pressione Enter para voltar ao menu.";
    
	getline(cin, id);
	mostraMembros();
}
