#include "menu.h"

sqlite3 *db;
char *zErrMsg = 0;
int rc;

//==================================================================================================================================//
// Funções auxiliares
//==================================================================================================================================//
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++){
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

void limpaBanco()
{
    sqlite3_stmt *stmt;
    
    sqlite3_prepare( db, "DELETE FROM MEMBROS;", -1, &stmt, 0);
    sqlite3_step(stmt);
    
    sqlite3_prepare( db, "DELETE FROM DESPESAS;", -1, &stmt, 0);
    sqlite3_step(stmt);
    
    sqlite3_prepare( db, "DELETE FROM CREDITOS;", -1, &stmt, 0);
    sqlite3_step(stmt);
    
    sqlite3_prepare( db, "DROP TABLE MEMBROS;", -1, &stmt, 0);
    sqlite3_step(stmt);
    
    sqlite3_prepare( db, "DROP TABLE DESPESAS;", -1, &stmt, 0);
    sqlite3_step(stmt);
    
    sqlite3_prepare( db, "DROP TABLE MEMBROS;", -1, &stmt, 0);
    sqlite3_step(stmt);
}

bool conectaBD()
{
    rc = sqlite3_open("financas.db", &db);
    
    if( rc ){
        fprintf(stderr, "Não foi possivel acessar o banco de dados: %s\n", sqlite3_errmsg(db));
        return false;
    }
    
    return true;
}

// function expects the string in format dd/mm/yyyy:
bool extraiData(const std::string& s, int& d, int& m, int& y){
    std::istringstream is(s);
    char delimiter;
    if (is >> d >> delimiter >> m >> delimiter >> y) {
        struct tm t = {0};
        t.tm_mday = d;
        t.tm_mon = m - 1;
        t.tm_year = y - 1900;
        t.tm_isdst = -1;
        
        // normalize:
        time_t when = mktime(&t);
        const struct tm *norm = localtime(&when);
        // the actual date would be:
        // m = norm->tm_mon + 1;
        // d = norm->tm_mday;
        // y = norm->tm_year;
        // e.g. 29/02/2013 would become 01/03/2013
        
        // validate (is the normalized date still the same?):
        return (norm->tm_mday == d    &&
                norm->tm_mon  == m - 1 &&
                norm->tm_year == y - 1900);
    }
    return false;
}

bool validaDataMenor(s_data inicial, s_data final)
{
    if (inicial.a > final.a) return false;
    if (inicial.m > final.m) return false;
    if (inicial.d > final.d) return false;
    return true;
//    return (inicial.a > final.a) && (inicial.m > final.m) && (inicial.d > final.d);
}
//==================================================================================================================================//
// Fim das funções auxiliares
//==================================================================================================================================//


//==================================================================================================================================//
// Funções privadas de criação de tabelas
//==================================================================================================================================//
bool criaTabelaMembros()
{
    char *sql;
    
    sql = "CREATE TABLE IF NOT EXISTS MEMBROS("  \
    "ID      INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL," \
    "NOME    TEXT                                  NOT NULL," \
    "IDADE   INT                                   NOT NULL," \
    "TIPO    CHAR(1)                               NOT NULL);";
    
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "Erro ao executar commando: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }
    return true;
}

bool criaTabelaDespesas()
{
    char *sql;
    
    sql = "CREATE TABLE IF NOT EXISTS DESPESAS("  \
    "ID         INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL," \
    "DESCRICAO  TEXT                                  NULL," \
    "CATEGORIA  TEXT                                  NOT NULL," \
    "TIPO       CHAR(1)                               NOT NULL," \
    "DT_VENC    TEXT                                  NOT NULL," \
    "DT_PGTO    TEXT                                  NULL," \
    "VALOR      REAL                                  NOT NULL," \
    "VL_FERIAS  REAL                                  NULL," \
    "ID_MEMBRO  INTEGER                               NULL," \
    "ID_CREDITO INTEGER                               NULL," \
    "FOREIGN    KEY(ID_MEMBRO)          REFERENCES MEMBROS(ID)," \
    "FOREIGN    KEY(ID_CREDITO)         REFERENCES CREDITOS(ID));";
    
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "Erro ao executar commando: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }
    return true;
}

bool criaTabelaCreditos()
{
    char *sql;
    
    sql = "CREATE TABLE IF NOT EXISTS CREDITOS("  \
    "ID         INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL," \
    "DESCRICAO  TEXT                                  NULL," \
    "TIPO       CHAR(1)                               NOT NULL," \
    "DT_RECEB   TEXT                                  NOT NULL," \
    "DT_DEVOL   TEXT                                  NULL," \
    "VALOR_P    REAL                                  NULL," \
    "VALOR      REAL                                  NOT NULL," \
    "ID_MEMBRO  INTEGER                               NOT NULL," \
    "FOREIGN    KEY(ID_MEMBRO)          REFERENCES MEMBROS(ID));";
    
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "Erro ao executar commando: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }
    return true;
}
//==================================================================================================================================//
// Fim das funções de criação de tabelas
//==================================================================================================================================//


//==================================================================================================================================//
// Funções privadas dos Membros Familiares
//==================================================================================================================================//

/*!
 * @discussion Retorna a lista de membros familiares presente no
 * banco de dados.
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linhas].
 */
std::vector<std::vector<std::string>> listaMembrosBD()
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 4; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM MEMBROS;", -1, &stmt, NULL );
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 4; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Retorna um membro familiare presente no banco
 * de dados, baseado no id passado de parametro.
 *
 * @param id (int) Id do membro familiar
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linha].
 */
std::vector<std::vector<std::string>> retornaMembroBD(int id)
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 4; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM MEMBROS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 4; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Insere um membro familiar no banco de dados.
 *
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool insereMembroBD(const char* nome, int idade, const char* tipo)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "INSERT INTO MEMBROS (ID,NOME,IDADE,TIPO) VALUES (NULL, ?, ?, ?)", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, nome, (unsigned)strlen(nome), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro nome.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 2, idade) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro idade.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel inserir o registro de Membro Familiar.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Altera um membro familiar no banco de dados,
 * baseado no id passado como parametro.
 *
 * @param id    (int)          Id do membro familiar
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool alteraMembroBD(int id, const char* nome, int idade, const char* tipo)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "UPDATE MEMBROS SET NOME = ?, IDADE = ?, TIPO = ? WHERE ID = ?;", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, nome, (unsigned)strlen(nome), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro nome.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 2, idade) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro idade.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 4, id) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro id.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel alterar o registro de Membro Familiar.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Remove um membro familiar no banco de dados,
 * baseado no id passado como parametro.
 *
 * @param id (int) Id do membro familiar
 *
 * @return bool
 */
bool excluiMembroBD(int id)
{
    sqlite3_stmt *stmt;
    
    sqlite3_prepare( db, "DELETE FROM MEMBROS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    if (sqlite3_step(stmt) != SQLITE_DONE) return false;
    
    return true;
}
//==================================================================================================================================//
// Fim das funções privadas dos Membros Familiares
//==================================================================================================================================//

//==================================================================================================================================//
// Funções privadas das Despesas
//==================================================================================================================================//

/*!
 * @discussion Retorna a lista de despesas presente no banco de dados.
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linhas].
 */
std::vector<std::vector<std::string>> listaDespesasBD()
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 10; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM DESPESAS;", -1, &stmt, NULL );
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 10; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Retorna uma despesa presente no banco de dados,
 * baseado no id passado de parametro.
 *
 * @param id (int) Id da despesa
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linha].
 */
std::vector<std::vector<std::string>> retornaDespesaBD(int id)
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 10; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM DESPESAS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 10; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Insere uma despesa no banco de dados.
 *
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool insereDespesaBD(const char* descricao, const char* categoria, const char* tipo, const char* dt_venc, const char* dt_pgto,
                     float valor, float vl_ferias, int id_membro, int id_credito)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "INSERT INTO DESPESAS (ID,DESCRICAO,CATEGORIA,TIPO,DT_VENC,DT_PGTO,VALOR,VL_FERIAS,ID_MEMBRO,ID_CREDITO) " \
                            "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, descricao, (unsigned)strlen(descricao), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro descricao.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 2, categoria, (unsigned)strlen(categoria), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro categoria.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 4, dt_venc, (unsigned)strlen(dt_venc), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de vencimento.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 5, dt_pgto, (unsigned)strlen(dt_pgto), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de pagamento.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 6, valor) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 7, vl_ferias) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 8, id_membro) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro membro familiar.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 9, id_credito) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro credito.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel inserir o registro de Despesa.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Altera uma despesa no banco de dados,
 * baseado no id passado como parametro.
 *
 * @param id    (int)          Id da despesa
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool alteraDespesaBD(int id, const char* descricao, const char* categoria, const char* tipo, const char* dt_venc, const char* dt_pgto,
                     float valor, float vl_ferias, int id_membro, int id_credito)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "UPDATE DESPESAS SET DESCRICAO = ?, CATEGORIA = ?, TIPO = ?, DT_VENC = ?, DT_PGTO = ?, " \
                            "VALOR = ?, VL_FERIAS = ?, ID_MEMBRO = ?, ID_CREDITO = ? WHERE ID = ?;", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, descricao, (unsigned)strlen(descricao), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro descricao.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 2, categoria, (unsigned)strlen(categoria), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro categoria.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 4, dt_venc, (unsigned)strlen(dt_venc), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de vencimento.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 5, dt_pgto, (unsigned)strlen(dt_pgto), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de pagamento.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 6, valor) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 7, vl_ferias) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 8, id_membro) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro membro familiar.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 9, id_credito) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro credito.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 10, id) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro id.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel alterar o registro de Despesa.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Remove uma despesa no banco de dados, baseado no id passado como parametro.
 *
 * @param id (int)   Id da despesa
 *
 * @return bool
 */
bool excluiDespesaBD(int id)
{
    sqlite3_stmt *stmt;
    
    sqlite3_prepare( db, "DELETE FROM DESPESAS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    if (sqlite3_step(stmt) != SQLITE_DONE) return false;
    
    return true;
}
//==================================================================================================================================//
// Fim das funções privadas de Despesas
//==================================================================================================================================//


//==================================================================================================================================//
// Funções privadas dos Créditos
//==================================================================================================================================//

/*!
 * @discussion Retorna a lista de creditos presente no banco de dados.
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linhas].
 */
std::vector<std::vector<std::string>> listaCreditosBD()
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 8; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM CREDITOS;", -1, &stmt, NULL );
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 8; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Retorna um credito presente no banco de dados,
 * baseado no id passado de parametro.
 *
 * @param id (int) Id da despesa
 *
 * @return vector<vector<string>> Vetor contendo resultado[colunas][linha].
 */
std::vector<std::vector<std::string>> retornaCreditoBD(int id)
{
    sqlite3_stmt *stmt;
    std::vector<std::vector<std::string>> resultado;
    for( int i = 0; i < 8; i++ )
        resultado.push_back(std::vector< std::string >());
    
    sqlite3_prepare( db, "SELECT * FROM CREDITOS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    sqlite3_step(stmt);
    
    while( sqlite3_column_text(stmt, 0))
    {
        for( int i = 0; i < 8; i++ )
            resultado[i].push_back( std::string( (char *)sqlite3_column_text( stmt, i ) ) );
        sqlite3_step( stmt );
    }
    
    return resultado;
}

/*!
 * @discussion Insere um credito no banco de dados.
 *
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool insereCreditoBD(const char* descricao, const char* tipo, const char* dt_receb, const char* dt_devol,
                     float valor, float valor_p, int id_membro)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "INSERT INTO CREDITOS (ID,DESCRICAO,TIPO,DT_RECEB,DT_DEVOL,VALOR,VALOR_P,ID_MEMBRO) " \
                        "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, descricao, (unsigned)strlen(descricao), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro descricao.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 2, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, dt_receb, (unsigned)strlen(dt_receb), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de recebimento.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 4, dt_devol, (unsigned)strlen(dt_devol), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de devolução.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 5, valor) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 6, valor_p) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor poupado.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 7, id_membro) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro membro familiar.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel inserir o registro de Credito.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Altera um credito no banco de dados,
 * baseado no id passado como parametro.
 *
 * @param id    (int)          Id da despesa
 * @param nome  (const char*)  Nome do membro familiar
 * @param idade (int)          Idade do membro familiar
 * @param tipo  (const char*)  Grupo do membro familiar (P ou F)
 *
 * @return bool
 */
bool alteraCreditoBD(int id, const char* descricao, const char* tipo, const char* dt_receb, const char* dt_devol,
                     float valor, float valor_p, int id_membro)
{
    sqlite3_stmt *stmt;
    
    if (sqlite3_prepare(db, "UPDATE DESPESAS SET DESCRICAO = ?, TIPO = ?, DT_RECEB = ?, DT_DEVOL = ?, " \
                        "VALOR = ?, VALOR_P = ?, ID_MEMBRO = ? WHERE ID = ?;", -1, &stmt, 0) != SQLITE_OK)
    {
        printf("\nNão foi possivel executar a operação.");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 1, descricao, (unsigned)strlen(descricao), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro descricao.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 2, tipo, 1, SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro tipo.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 3, dt_receb, (unsigned)strlen(dt_receb), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de recebimento.\n");
        return false;
    }
    
    if (sqlite3_bind_text(stmt, 4, dt_devol, (unsigned)strlen(dt_devol), SQLITE_TRANSIENT) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro data de devolução.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 5, valor) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor.\n");
        return false;
    }
    
    if (sqlite3_bind_double(stmt, 6, valor_p) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro valor poupado.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 7, id_membro) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro membro familiar.\n");
        return false;
    }
    
    if (sqlite3_bind_int(stmt, 8, id) != SQLITE_OK)
    {
        printf("\nNão foi possivel fazer bind do parametro id.\n");
        return false;
    }
    
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        printf("\nNão foi possivel alterar o registro de Credito.\n");
        return false;
    }
    
    sqlite3_reset(stmt);
    return true;
}

/*!
 * @discussion Remove um credito no banco de dados, baseado no id passado como parametro.
 *
 * @param id (int)   Id do credito
 *
 * @return bool
 */
bool excluiCreditoBD(int id)
{
    sqlite3_stmt *stmt;
    
    sqlite3_prepare( db, "DELETE FROM CREDITOS WHERE ID = ?;", -1, &stmt, 0);
    sqlite3_bind_int(stmt, 1, id);
    if (sqlite3_step(stmt) != SQLITE_DONE) return false;
    
    return true;
}
//==================================================================================================================================//
// Fim das funções privadas de Despesas
//==================================================================================================================================//





//==================================================================================================================================//
// Funções publicas do repositório.
//==================================================================================================================================//
void criarTabelas()
{
    if (!conectaBD() || !criaTabelaMembros() || !criaTabelaCreditos() || !criaTabelaDespesas()) std::cout << "Erro ao criar tabelas no banco de dados." << std::endl;
    sqlite3_close(db);
}


bool listarRegistros(int op, std::vector<std::vector<std::string>> &retorno)
{
    if (conectaBD())
    {
        if (op == 1)
        {
            retorno = listaMembrosBD();
            if (retorno[0].size() <= 0) return false;
        }
        else if (op == 2)
        {
            retorno = listaDespesasBD();
            if (retorno[0].size() <= 0) return false;
        }
        else if (op == 3)
        {
            retorno = listaCreditosBD();
            if (retorno[0].size() <= 0) return false;
        }
    }
    sqlite3_close(db);
    return true;
}

bool retornaRegistro(int op, int id, std::vector<std::vector<std::string>> &retorno)
{
    if (conectaBD())
    {
        if (op == 1)
        {
            retorno = retornaMembroBD(id);
            if (retorno[0].size() <= 0) return false;
        }
        else if (op == 2)
        {
            retorno = retornaDespesaBD(id);
            if (retorno[0].size() <= 0) return false;
        }
        else if (op == 3)
        {
            retorno = retornaCreditoBD(id);
            if (retorno[0].size() <= 0) return false;
        }
    }
    sqlite3_close(db);
    return true;
}

//bool inserirRegistro(int op, const char* nome, int idade, const char* tipo)
bool inserirRegistro(int op, s_membro membro, s_despesa despesa, s_credito credito)
{
    bool retorno = false;
    
    if (conectaBD())
    {
        if (op == 1) retorno = insereMembroBD(membro.nome, membro.idade, membro.tipo);
        else if (op == 2) retorno = insereDespesaBD(despesa.descricao, despesa.categoria,
                                                    despesa.tipo, despesa.dt_venc, despesa.dt_pgto,
                                                    despesa.valor, despesa.vl_ferias, despesa.id_membro,
                                                    despesa.id_credito);
        else if (op == 3) retorno = insereCreditoBD(credito.descricao, credito.tipo,
                                                    credito.dt_receb, credito.dt_devol,
                                                    credito.valor, credito.valor_p,
                                                    credito.id_membro);
    }
    sqlite3_close(db);
    return retorno;
}

bool alterarRegistro(int op, s_membro membro, s_despesa despesa, s_credito credito)
{
    bool retorno = false;
    
    if (conectaBD())
    {
        if (op == 1) retorno = alteraMembroBD(membro.id, membro.nome, membro.idade, membro.tipo);
        else if (op == 2) retorno = alteraDespesaBD(despesa.id, despesa.descricao, despesa.categoria,
                                                    despesa.tipo, despesa.dt_venc, despesa.dt_pgto,
                                                    despesa.valor, despesa.vl_ferias, despesa.id_membro,
                                                    despesa.id_credito);
        else if (op == 3) retorno = alteraCreditoBD(credito.id, credito.descricao, credito.tipo,
                                                    credito.dt_receb, credito.dt_devol,
                                                    credito.valor, credito.valor_p,
                                                    credito.id_membro);
    }
    sqlite3_close(db);
    return retorno;
}

bool excluirRegistro(int op, int id)
{
    bool retorno = false;
    
    if (conectaBD())
    {
        if (op == 1) retorno = excluiMembroBD(id);
        else if (op == 2) retorno = excluiDespesaBD(id);
        else if (op == 3) retorno = excluiCreditoBD(id);
    }
    sqlite3_close(db);
    return retorno;

}
//==================================================================================================================================//
// Fim das funções publicas do repositório.
//==================================================================================================================================//