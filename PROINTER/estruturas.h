//
//  estruturas.h
//  PROINTER
//
//  Created by Jonathan Willian on 20/08/16.
//  Copyright © 2016 Akamura Asai. All rights reserved.
//

#ifndef estruturas_h
#define estruturas_h

struct s_membro {
    int id;
    const char* nome;
    int idade;
    const char* tipo;
};

struct s_despesa {
    int id;
    const char* descricao;
    const char* categoria;
    const char* tipo;
    const char* dt_venc;
    const char* dt_pgto;
    float valor;
    float vl_ferias;
    int id_membro;
    int id_credito;
};

struct s_credito {
    int id;
    const char* descricao;
    const char* tipo;
    const char* dt_receb;
    const char* dt_devol;
    float valor;
    float valor_p;
    int id_membro;
};

struct s_data {
    int d;
    int m;
    int a;
};

#endif /* estruturas_h */
