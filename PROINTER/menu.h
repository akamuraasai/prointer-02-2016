#define iostream
#include <iostream>
#include <iomanip>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <locale.h>
#include <algorithm>
#include <vector>
#include <stdio.h>
#include <sqlite3.h>
#include <ctime>
#include <math.h>
#include "membros.h"
#include "despesas.h"
#include "creditos.h"
#include "relatorios.h"
#include "repositorio.h"

// Funções que geram visual no console.
void mostraMenuPrincipal();
void mostraMembros();
void mostraDespesas();
void mostraCreditos();
void mostraRelatorios();

// Funcões que determinam as ações de acordo com o menu e a opção escolhida.
bool opcoesPrincipal(int opt);
bool opcoesMembros(int opt);

// Imprime os itens do menu.
void imprimeItens(std::string item);

// Chama o switch de que irá determinar ação escolhida, baseado no menu atual e na opção escolhida.
bool chamaSwitch(int opt, int menu);

// Monta o visual do menu.
void montaMenu(std::string bread, std::vector<std::string> itens, int menu);

// Mostra o cabeçalho do programa.
void mostraCabecalho(std::string item = "");

// Limpa o console, independente do sistema operacional.
void limpaConsole();